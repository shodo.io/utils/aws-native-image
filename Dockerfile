# Image used to build the native application AWS artifact in a container with an OS compatible with Amazon Linux.

FROM public.ecr.aws/amazonlinux/amazonlinux:2.0.20240318.0-amd64

ARG JDK_SDKMAN_VERSION
ENV JDK_SDKMAN_VERSION=$JDK_SDKMAN_VERSION

ARG GRADLE_VERSION
ENV GRADLE_VERSION=$GRADLE_VERSION

RUN yum -y update \
    && yum install -y zip unzip tar gzip bzip2-devel ed gcc gcc-c++ gcc-gfortran \
    less libcurl-devel openssl openssl-devel readline-devel xz-devel \
    zlib-devel glibc-static libcxx libcxx-devel llvm-toolset-7 zlib-static \
    gettext \
    && rm -rf /var/cache/yum

# Install SDKMan and Java GraalVM
RUN curl -s "https://get.sdkman.io" | bash
RUN source "$HOME/.sdkman/bin/sdkman-init.sh" && sdk install java $JDK_SDKMAN_VERSION
ENV JAVA_HOME "/root/.sdkman/candidates/java/current"

# Download and install Gradle
RUN \
    cd /usr/local && \
    curl -L https://services.gradle.org/distributions/gradle-$GRADLE_VERSION-bin.zip -o gradle-$GRADLE_VERSION-bin.zip && \
    unzip gradle-$GRADLE_VERSION-bin.zip && \
    rm gradle-$GRADLE_VERSION-bin.zip

# Export some environment variables
ENV GRADLE_HOME=/usr/local/gradle-$GRADLE_VERSION
ENV PATH=$PATH:$GRADLE_HOME/bin

# Install AWS Lambda Builders
RUN amazon-linux-extras enable python3.8
RUN yum clean metadata && yum -y install python3.8
RUN curl -L get-pip.io | python3.8
RUN pip3 install aws-lambda-builders
RUN yum install -y python38 python38-devel
RUN pip3 install awscli --upgrade
RUN pip3 install aws-sam-cli --upgrade