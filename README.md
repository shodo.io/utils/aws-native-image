# Compilation native

Ce projet contient une image docker permettant de compiler nativement via GraalVm
du code kotlin et de le publier sur une lambda aws

## Aperçu

Le pipeline GitLab CI de ce projet est configuré pour construire automatiquement l'image Docker chaque fois que des modifications sont poussées dans le dépôt.
Il pousse ensuite les images construites vers le GitLab Container Registry.

## Mise à jour

Pour mettre à jour la version de la jdk et ou celle de gradle, référez vous à la section `variables` de `.gitlab-ci.yml`